# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.3.3](https://gitee.com/dh1992/dux-ui-react/compare/v1.3.2...v1.3.3) (2022-05-14)

### Bug Fixes

- adapt vite project and bugfix ([bbe190d](https://gitee.com/dh1992/dux-ui-react/commit/bbe190dda1ad476df1ea5e763154addc2ab32ffa))

### [1.3.2](https://gitee.com/dh1992/dux-ui-react/compare/v1.3.1...v1.3.2) (2022-05-14)

### Bug Fixes

- publish bug fix ([ba2b6fa](https://gitee.com/dh1992/dux-ui-react/commit/ba2b6fa1b813da10b3c01ae04a0ac52e80cb08c1))

### [1.3.1](https://gitee.com/dh1992/dux-ui-react/compare/v1.3.0...v1.3.1) (2022-05-14)

## [1.3.0](https://gitee.com/dh1992/dux-ui-react/compare/v1.2.0...v1.3.0) (2022-05-14)

### Features

- add modal ([5935d6f](https://gitee.com/dh1992/dux-ui-react/commit/5935d6f69d993c03d3be25f267b874f90fdbafbe))
- add table, base function ([f77738c](https://gitee.com/dh1992/dux-ui-react/commit/f77738c78fe05f5a25e4d1ab1b50ae9ffead1c79))
- add Tooltip ([3d93870](https://gitee.com/dh1992/dux-ui-react/commit/3d938705b9edc3f122e68930fda565d0d6721710))
- add Upload ([7833c7d](https://gitee.com/dh1992/dux-ui-react/commit/7833c7dddd77ae87a1cc47af9e302bbbd984edde))
- Table finish ([a2dd8b8](https://gitee.com/dh1992/dux-ui-react/commit/a2dd8b868ce28ba5e9647a74ab60c3d95e58c9ed))

## [1.2.0](https://gitee.com/dh1992/dux-ui-react/compare/v1.1.1...v1.2.0) (2022-05-11)

### Features

- add DatePicker ([7dc6eb6](https://gitee.com/dh1992/dux-ui-react/commit/7dc6eb665076d1ca60565503aed60d0f865d74b3))
- add Form Component: Imput/InputNumber/Select/Switch/Tooltip ([8cf41db](https://gitee.com/dh1992/dux-ui-react/commit/8cf41db52dacefcfd8d98f7ebb35e0a6e110d8a1))
- add form validate and add Checkbox/Radio/Switch/Menu ([3967d46](https://gitee.com/dh1992/dux-ui-react/commit/3967d465a5566a3201c25bc88e2389a9eda1b698))

### Bug Fixes

- bugfix ([7d3b096](https://gitee.com/dh1992/dux-ui-react/commit/7d3b0964e442e17c0ec28242a03fd6bba6c1071b))
- bugfix ([7ba2d18](https://gitee.com/dh1992/dux-ui-react/commit/7ba2d18a77eea3afc797cc1282cbec4a7df6b5d3))
- conflict ([0cceb23](https://gitee.com/dh1992/dux-ui-react/commit/0cceb23d8b540230f328303b4a08263fed8a1314))

### 1.1.1 (2022-04-03)

### Features

- bugfix ([1cf6da5](https://gitee.com/dh1992/dux-ui-react/commit/1cf6da5d748507c91573d3982ea0c57d67549976))

## [1.1.0](https://gitee.com/dh1992/dux-ui-react/compare/v1.0.12...v1.1.0) (2022-04-03)

### Features

- **box and combine:** add Box and Combine ([9d03d3e](https://gitee.com/dh1992/dux-ui-react/commit/9d03d3e93d1f0622626272f5ffb60e2dcf9b4e88))

### [1.0.12](https://gitee.com/dh1992/dux-ui-react/compare/v1.0.11...v1.0.12) (2022-04-03)

### Bug Fixes

- bugfix: css bug ([57dfe96](https://gitee.com/dh1992/dux-ui-react/commit/57dfe9670814b25d72584094d798ca744009f86a))
- bugfix: css bug ([bd9ed40](https://gitee.com/dh1992/dux-ui-react/commit/bd9ed402ea5e7e8d86a9a9ae462213cb705d5284))

### [1.0.11](https://gitee.com/dh1992/dux-ui-react/compare/v1.0.10...v1.0.11) (2022-04-01)

### Bug Fixes

- fix webpack config ([342bb5c](https://gitee.com/dh1992/dux-ui-react/commit/342bb5c47a58d4801722f5db02ee8d60e6c48067))

### [1.0.10](https://gitee.com/dh1992/dux-ui-react/compare/v1.0.9...v1.0.10) (2022-03-28)

### Features

- add changelog and fix icon style bug ([30def64](https://gitee.com/dh1992/dux-ui-react/commit/30def64891d000f038643eadfdfe3552f3c287f3))
- **add git-cz and change icon style:** add git-cz and change icon st ([2a8ff17](https://gitee.com/dh1992/dux-ui-react/commit/2a8ff179c395757ff72cf7090ca5b1050acdc1f2))
