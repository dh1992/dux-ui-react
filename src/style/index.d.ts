/** @deprecated */
export declare const clearFixMixin: import('_@emotion_utils@0.11.3@@emotion/utils').SerializedStyles;
/** @deprecated */
export declare const calculateSize: (size: string, offset: number) => string;
export declare const inlineBlockWithVerticalMixin: import('_@emotion_utils@0.11.3@@emotion/utils').SerializedStyles;
export declare const spinMixin: import('_@emotion_utils@0.11.3@@emotion/utils').SerializedStyles;
export declare const spinPulseMixin: import('_@emotion_utils@0.11.3@@emotion/utils').SerializedStyles;
export * from './interface';
export * from './utils';
export * from './animation';
export { default as styleWrap } from './styleWrap';
