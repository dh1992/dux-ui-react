export declare const itemCls: string;
export declare const separatorCls: string;
export declare const CombineWrap: import('react').ForwardRefExoticComponent<
  {
    spacing: string;
  } & import('react').HTMLAttributes<HTMLElement> & {
      theme?: import('../../../style').Theme | undefined;
    } & import('react').RefAttributes<HTMLElement>
>;
