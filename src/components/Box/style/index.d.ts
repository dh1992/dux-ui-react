export declare const BoxWrap: import('react').ForwardRefExoticComponent<
  import('react').HTMLAttributes<HTMLElement> & {
    theme?: import('../../../style').Theme | undefined;
  } & import('react').RefAttributes<HTMLElement>
>;
