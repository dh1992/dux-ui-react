---
group:
  title: 表单组件
  path: /components/formEle
  order: 1

order: 3
---

# Textarea

```tsx
import React from 'react';
import { Textarea } from 'dux-ui';

// demo start
const Demo = () => (
  <div>
    <div className="demo-wrap">
      <Textarea placeholder="please input here" />
    </div>
  </div>
);
// demo end

export default Demo;
```

<API></API>
