export declare const StyleTypes: [
  'primary',
  'warning',
  'success',
  'error',
  'border',
  'border-gray',
];
export declare const Sizes: ['sm', 'md', 'lg'];
export declare const Shapes: ['circle', 'square'];
