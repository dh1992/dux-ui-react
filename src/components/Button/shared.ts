import { tuple } from '../../style';

export const StyleTypes = tuple('primary', 'warning', 'success', 'error', 'border', 'border-gray');
export const Sizes = tuple('sm', 'md', 'lg');
export const Shapes = tuple('circle', 'square');
