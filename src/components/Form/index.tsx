import ZForm from './ZForm';
export default ZForm;

import controllerDecorator from './controllerDecorator';
(ZForm as any).controllerDecorator = controllerDecorator;
