import Form from './Form';
export default Form;

import Item from './Item';
(Form as any).Item = Item;

import Group from './Group';
(Form as any).Group = Group;

import SubArea from './SubArea';
(Form as any).SubArea = SubArea;
