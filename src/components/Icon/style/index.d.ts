export declare const prefixCls: string;
declare type SpinProps = {
  pulse: boolean;
};
export declare const IconWrap: import('react').ForwardRefExoticComponent<
  {
    spin?: SpinProps | undefined;
  } & import('react').HTMLAttributes<HTMLElement> & {
      theme?: import('../../../style').Theme | undefined;
    } & import('react').RefAttributes<HTMLElement>
>;
export {};
