import Radio from './Radio';
export default Radio;

import Group from './Group';
(Radio as any).Group = Group;

import Button from './Button';
(Radio as any).Button = Button;

import Tag from './Tag';
(Radio as any).Tag = Tag;
