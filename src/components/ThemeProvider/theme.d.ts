import { designTokens } from './designTokens';
export declare const extend: (source: any, target: any) => any;
export declare const generateTheme: (originTheme?: any) => any;
declare const defaultTheme: any;
export default defaultTheme;
export { defaultTheme };
export { designTokens as defaultDesignTokens };
