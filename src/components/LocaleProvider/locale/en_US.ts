import Pagination from '../components/Pagination/locale/en_US';
import DatePicker from '../../DatePicker/locale/en_US';
import Calendar from '../../Calendar/locale/en_US';
import Select from '../../Select/locale/en_US';
import Upload from '../../Upload/locale/en_US';
import Slider from '../../Slider/locale/en_US';
import Modal from '../../Modal/locale/en_US';
import Table from '../../Table/locale/en_US';
import Menu from '../../Menu/locale/en_US';
import PopConfirm from '../../PopConfirm/locale/en_US';
import EditableList from '../../EditableList/locale/en_US';
import Transfer from '../../Transfer/locale/en_US';
import SharedSearch from 'src/sharedComponents/Search/locale/en_US';

export default {
  locale: 'en',
  Pagination,
  DatePicker,
  Calendar,
  Select,
  Upload,
  Slider,
  Modal,
  Table,
  Menu,
  PopConfirm,
  EditableList,
  Transfer,
  SharedSearch,
};
