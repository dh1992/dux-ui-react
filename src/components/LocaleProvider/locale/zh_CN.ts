import Pagination from '../../Pagination/locale/zh_CN';
import DatePicker from '../../DatePicker/locale/zh_CN';
import Calendar from '../../Calendar/locale/zh_CN';
import Select from '../../Select/locale/zh_CN';
import Upload from '../../Upload/locale/zh_CN';
import Slider from '../../Slider/locale/zh_CN';
import Modal from '../../Modal/locale/zh_CN';
import Table from '../../Table/locale/zh_CN';
import Menu from '../../Menu/locale/zh_CN';
import PopConfirm from '../../PopConfirm/locale/zh_CN';
import EditableList from '../../EditableList/locale/zh_CN';
import Transfer from '../../Transfer/locale/zh_CN';
import SharedSearch from 'src/sharedComponents/Search/locale/zh_CN';

export default {
  locale: 'zh-cn',
  Pagination,
  DatePicker,
  Calendar,
  Select,
  Upload,
  Slider,
  Modal,
  Table,
  Menu,
  PopConfirm,
  EditableList,
  Transfer,
  SharedSearch,
};
